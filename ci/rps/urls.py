from django.conf.urls import url
from django.urls import path

from rps import views


urlpatterns = [
    path('hello-world/', views.helloWorld, name='hello-world')
]
